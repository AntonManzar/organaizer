package ru.organaizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganaizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrganaizerApplication.class, args);
	}

}
